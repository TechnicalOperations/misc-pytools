from snowflake import connector
from os.path import expanduser


param_map = {
    'accountname': 'account',
    'username': 'user',
    'password': 'password',
    'dbname': 'database',
    'rolename': 'role',
}

con = None


def read_params():
    with open(expanduser('~')+'/.snowsql/config', 'r') as f:
        params = f.read()
    params = [i.split('=') for i in params.split('\n')]
    params = {param_map[i[0]]: i[1] for i in params if i[0] in param_map}
    return params


def connect():
    global con
    params = read_params()
    con = connector.connect(**params)
    return con


def close():
    global con
    con.close()
