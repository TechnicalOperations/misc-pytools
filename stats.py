import numpy as np
import pandas


def calc_stats(x, cols):
    out = pandas.Series(index=['avg', 'std', cols[-1]])
    out[cols[-1]] = x[cols[-1]].sum()
    out['avg'] = (x[cols[-2]] * x[cols[-1]]).sum() / out[cols[-1]]
    std = (x[cols[-2]] - out['avg']) ** 2
    out['std'] = np.sqrt(std.sum() / out[cols[-1]])
    return out


def weighted_stats(data, index, metric, count):
    if count is None:
        count = 'count'
        data[count] = 1
    if type(index) == list:
        cols = index + [metric, count]
    else:
        cols = [index, metric, count]
    data = data.copy()
    data = data[cols].groupby(index+[metric] if type(index) == list else [index, metric]).sum()
    l = len(index+[metric]) if type(index) == list else 2
    l = l - 1
    data = data.reset_index(level=l)
    data = data.groupby(level=[i for i in range(l)]).apply(lambda x: calc_stats(x, cols))
    data[count] = data[count].apply(int)
    return data
