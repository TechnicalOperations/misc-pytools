import os
import requests
import json
from . import sql
import pandas


LOGIN = 'https://dashboard.rhythmxchange.com/api/login'
DSP = 'http://dashboard.rhythmxchange.com/api/dsp/view/{0}'
SSP = 'https://dashboard.rhythmxchange.com/api/ssp/view/{0}'

DEAL = 'SELECT * FROM ods.rx.deal WHERE deal_id = {0}'
DEAL_SSP = 'SELECT * FROM ods.rx.deal_ssp WHERE deal_id = {0}'
DEAL_DSP = 'SELECT * FROM ods.rx.deal_dsp WHERE deal_id = {0}'
DEAL_SEGMENT = 'SELECT * FROM ods.rx.deal_segment WHERE deal_id = {0}'
SSP_NAME = 'SELECT id, name FROM ods.rx.ssp WHERE id IN ({0})'
DSP_NAME = 'SELECT id, name FROM ods.rx.dsp WHERE id IN ({0})'


class DashboardAPI:

    def __init__(self, credentials=os.path.expanduser('~/.dashpass')):
        with open(credentials, 'r') as f:
            cred = json.loads(f.read())
        res = requests.post(LOGIN, headers=cred)
        self.res = json.loads(res.text)
        self.api_key = self.res['api_key']
        self.headers = {'apikey': self.api_key}

    def get_dsp(self, dsp_id):
        dsp = requests.post(DSP.format(dsp_id), headers=self.headers)
        return json.loads(dsp.text)

    def get_ssp(self, ssp_id):
        ssp = requests.post(SSP.format(ssp_id), headers=self.headers)
        return json.loads(ssp.text)

    def read_query(self, query, con):
        data = pandas.read_sql_query(query, con)
        data = data.rename(columns={i: i.lower() for i in list(data.columns)})
        data = data.to_dict(orient='records')
        return data

    def get_deal(self, deal_id, verbose=False):
        con = sql.connect()
        deal = self.read_query(DEAL.format(deal_id), con)[0]
        try:
            deal['ssp'] = self.read_query(DEAL_SSP.format(deal_id), con)
            deal['ssp'] = [str(i['ssp_id']) for i in deal['ssp']]
            ssp_ids = deal['ssp']
            deal['ssp'] = self.read_query(SSP_NAME.format(", ".join(deal['ssp'])), con)
            deal['ssp'] = [i['name'] for i in deal['ssp']]
        except:
            deal['ssp'] = []
        deal['dsp'] = self.read_query(DEAL_DSP.format(deal_id), con)
        deal['dsp'] = [str(i['dsp_id']) for i in deal['dsp']]
        dsp_ids = deal['dsp']
        deal['dsp'] = self.read_query(DSP_NAME.format(", ".join(deal['dsp'])), con)
        deal['dsp'] = [i['name'] for i in deal['dsp']]
        deal['segment'] = self.read_query(DEAL_SEGMENT.format(deal_id), con)
        sql.close()
        deal['filter'] = {}
        filt = deal['filter']
        filt['adsize'] = {
            'type': deal['filter_adsize'],
            'list': [k for k in json.loads(deal['filter_adsize_list'])] if deal['filter_adsize_list'] is not None else []
        }
        filt['app'] = deal['filter_app']
        filt['banner'] = deal['filter_banner']
        filt['country'] = {
            'type': deal['filter_country'],
            'list': [k for k in json.loads(deal['filter_countrylist'])] if deal['filter_countrylist'] is not None else []
        }
        filt['display'] = deal['filter_display']
        filt['in_view_percent'] = deal['filter_inviewpercent']
        filt['mobile'] = deal['filter_mobile']
        filt['native'] = deal['filter_native']
        filt['site'] = deal['filter_site']
        deal['ssp'] = {
            'type': deal['filter_ssp'],
            'list': deal['ssp']
        }
        filt['video'] = deal['filter_video']
        filt['video_size'] = {
            'type': deal['filter_videosize'],
            'list': [k for k in json.loads(deal['filter_videosize_list'])] if deal['filter_videosize_list'] is not None else []
        }
        del deal['filter_ssp']
        del deal['filter_country']
        del deal['filter_countrylist']
        del deal['filter_adsize']
        del deal['filter_adsize_list']
        del deal['filter_videosize']
        del deal['filter_videosize_list']
        del deal['filter_banner']
        del deal['filter_video']
        del deal['filter_native']
        del deal['filter_display']
        del deal['filter_mobile']
        del deal['filter_site']
        del deal['filter_app']
        del deal['filter_inviewpercent']
        if verbose:
            return deal, ssp_ids, dsp_ids
        else:
            return deal
