import unittest
from imp import reload
from tools import pmp_filters
from datetime import datetime, timedelta


class PmpFilterTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.search_date = datetime.now() - timedelta(days=1)
        self.request_limit = 10
        self.pmp = pmp_filters.PmpFilter(limit_requests=self.request_limit, search_date=self.search_date)

    def test_make_segments(self):
        segments = [
            '["appid:72", "gv_grape", "siteid:85"]',
            '["appid:72", "gv_grape"]',
            '["gv_grape", "siteid:85"]',
            '["gv_grape", "siteid:nan"]'
        ]
        for i in range(len(segments)):
            segments[i] = pmp_filters.make_segments(segments[i])
        segments = [z for i in segments for z in i]
        self.assertEqual(len(segments), 4)

        invalid = "-"
        self.assertEqual(len(pmp_filters.make_segments(invalid)), 0)

    def test_comp_str_list(self):
        agg = {
            'column': {
                'v1': set([1, 2, 3, 4, 5]),
                'v2': set([3, 4, 5, 6, 7]),
                'v3': set([5, 6, 7, 8, 9])
            }
        }
        column = 'column'
        deal = ['v1', 'v2']
        ids = set([1, 4, 8, 9])

        list_type_include = 'include'
        include_ids = pmp_filters.comp_str_list(agg, column, deal, ids, list_type_include)
        self.assertEqual(include_ids, set([1, 4]))

        list_type_exclude = 'exclude'
        include_ids = pmp_filters.comp_str_list(agg, column, deal, ids, list_type_exclude)
        self.assertEqual(include_ids, set([8, 9]))

        list_type_none = None
        include_ids = pmp_filters.comp_str_list(agg, column, deal, ids, list_type_none)
        self.assertEqual(include_ids, set([1, 4]))

    def test_comp_list_list(self):
        agg = {
            'column': {
                'v1': set([1, 2, 3, 4, 5]),
                'v2': set([3, 4, 5, 6, 7, 10]),
                'v3': set([5, 6, 7, 8, 9])
            }
        }
        column = 'column'
        deal = ['v1', 'v3']
        ids = set([1, 4, 8, 9, 10])

        list_type_include = 'include'
        include_ids = pmp_filters.comp_list_list(agg, column, deal, ids, list_type_include)
        self.assertEqual(include_ids, set([1, 4, 8, 9]))

        list_type_exclude = 'exclude'
        include_ids = pmp_filters.comp_list_list(agg, column, deal, ids, list_type_exclude)
        self.assertEqual(include_ids, set([10]))

        list_type_none = None
        include_ids = pmp_filters.comp_list_list(agg, column, deal, ids, list_type_none)
        self.assertEqual(include_ids, set([1, 4, 8, 9]))

    def test_comp_float_float(self):
        agg = {
            'column': {
                40: set([1, 2, 3, 4, 5]),
                50: set([3, 4, 5, 6, 7, 10]),
                60: set([5, 6, 7, 8, 9])
            }
        }
        column = 'column'
        deal = 50
        ids = set([1, 4, 8, 9, 10])

        new_ids = pmp_filters.comp_float_float(agg, column, deal, ids, None)
        self.assertEqual(new_ids, set([4, 8, 9, 10]))

    def test_lower_dict_keys(self):
        item = [{
            'KEY1': 2,
            'KEY2': 0
        }]
        self.pmp.lower_dict_keys(item)
        self.assertEqual(len(item), 1)
        self.assertNotIn('KEY1', item[0])
        self.assertNotIn('KEY2', item[0])
        self.assertEqual(item[0]['key1'], 2)
        self.assertEqual(item[0]['key2'], 0)

    def test_get_deals(self):
        self.pmp.get_deals(self.search_date)
        self.assertTrue(len(self.pmp.deals) > 0)
        for item in self.pmp.deals:
            self.assertEqual(type(item), dict)
            for col in pmp_filters.PmpFilter.DEAL_COLUMNS:
                self.assertIn(col, item)

    def test_get_requests(self):
        self.pmp.get_requests(self.search_date)
        self.assertEqual(len(self.pmp.requests), self.request_limit)
        for k, v in self.pmp.requests.items():
            self.assertEqual(type(k), int)
            self.assertIn(type(v), [int, float])

    def test_make_results_template(self):
        self.pmp.make_results_template()
        for deal, values in self.pmp.results.items():
            for col in pmp_filters.PmpFilter.COLUMNS:
                self.assertEqual(values[col[1]['agg']['name']], 0)

    def test_process(self):
        self.pmp.process()
        for deal_id, values in self.pmp.results.items():
            for k, v in values.items():
                if k in ['deal_id', 'post_rg']:
                    continue
                self.assertGreater(values['post_rg']+1, v)

    def test_run_full_test(self):
        reload(pmp_filters)
        pmp = pmp_filters.PmpFilter(verbose=True, limit_requests=10)
        pmp.process()
        for deal_id, values in pmp.results.items():
            for k, v in values.items():
                if k in ['deal_id', 'post_rg']:
                    continue
                self.assertGreater(values['post_rg']+1, v)


if __name__ == "__main__":
    unittest.main()
