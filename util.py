import json
import pandas
import numpy as np
from copy import deepcopy
from matplotlib import pyplot as plt


def to_camel(x):
    x = x.lower().split('_')
    x = ''.join([x[0]] + [i[0].upper()+i[1:] for i in x[1:]])
    return x


def find_common(data):
    data = list(data)
    item = set(data[0])
    for i in data[1:]:
        item = item & set(i)
    return list(item)


def flatten(data, column):
    cols = list(data.columns)
    dict_data = data.to_dict(orient='records')
    out = []
    for item in dict_data:
        if len(item[column]) == 0:
            tmp = deepcopy(item)
            tmp[column] = None
            out.append(tmp)
        for i in item[column]:
            tmp = deepcopy(item)
            tmp[column] = i
            out.append(tmp)
    out = pandas.DataFrame(out)
    out = out[cols]
    return out


def find_key_path(data, key):
    if type(data) == list:
        if len(data) == 0:
            return []
        data = data[0]
    if type(data) != dict:
        return []
    if key in data:
        return [key]
    for k, v in data.items():
        print(k)
        n = find_key_path(v, key)
        if key in n:
            return [k] + n
    return []


def plot_multi(data, title_key=None):
    data = data.copy()
    data = pandas.pivot_table(data[['dspid', 'crid', 'impbeacon', 'impserved', 'revenue']], index=['crid'], columns=['dspid'], fill_value=0, aggfunc=np.sum)
    data.columns = data.columns.swaplevel(0, 1)
    keys = list(set([i[0] for i in list(data.columns)]))
    ax = None
    index = 0
    m = 0
    colors = ['blue', 'green', 'magenta', 'yellow', 'black', 'white']
    for key in keys:
        ax = data[key].plot(x='impserved', y='impbeacon', ax=ax, color=colors[index], kind='scatter')
        index += 1
        tmp = data[key]['impserved'].max() if data[key]['impserved'].max() < data[key]['impbeacon'].max() else data[key]['impbeacon'].max()
        m = tmp if tmp > m else m
    line = pandas.DataFrame(columns=['x', 'y'])
    line = line.append(pandas.Series({'x': 0, 'y': 0}), ignore_index=True)
    line = line.append(pandas.Series({'x': m, 'y': m}), ignore_index=True)
    line.plot(x='x', y='y', ax=ax, color='red')
    if title_key is not None:
        plt.title(title_key)
    plt.legend(['x=y'] + keys)
    plt.draw()


def convert_column(column, default=json.loads, create_tuple=False):
    data = set(column)
    data = {i: default(i) for i in data}
    return column.apply(lambda x: tuple(sorted(data[x])) if type(data[x]) == list and create_tuple else data[x])


def compare_dictionaries(left, right):
    diff = {}
    for k, v in left.items():
        if k not in right:
            diff[k] = (deepcopy(left[k]), None,)
            continue
        if type(v) == dict:
            diff[k] = compare_dictionaries(left[k], right[k])
            continue
        if type(v) == list:
            if bool(len(v) == 0) != bool(len(right[k]) == 0):
                diff[k] = (deepcopy(v), deepcopy(right[k]),)
                continue
            tmp = tuple(list(set([type(i) for i in v])))
            if tmp == (dict,):
                # I'll just compare only 1 dict for simplicity's sake for now
                diff[k] = compare_dictionaries(deepcopy(v[0]), deepcopy(right[k][0]))
                continue
            else:
                if tuple(sorted(list(set(v)))) != tuple(sorted(list(set(right[k])))):
                    diff[k] = (sorted(list(set(v))), sorted(list(set(right[k]))),)
                    continue
        if type(v) == str and type(right[k]) == str:
            if v.lower() != right[k].lower():
                diff[k] = (v, right[k],)
        else:
            if v != right[k]:
                diff[k] = (v, right[k],)
    return diff


def collect_values(item, out={}):
    for k, v in item.items():
        if k not in out:
            if type(v) in [dict, list]:
                out[k] = dict()
            else:
                out[k] = set()
        if type(v) == list:
            if len(v) > 0 and type(v[0]) not in [list, dict]:
                out[k] = set(out[k]) | set(v)
            else:
                for i in v:
                    out[k] = collect_values(i, out=out[k])
        elif type(v) == dict:
            out[k] = collect_values(v, out=out[k])
        else:
            out[k].add(v)
    return out


def collect_all_values(data):
    out = {}
    for item in data:
        for k, v in item.items():
            if k not in out:
                if type(v) in [dict, list]:
                    out[k] = dict()
                else:
                    out[k] = set()
            if type(v) == list:
                if len(v) > 0 and type(v[0]) not in [list, dict]:
                    out[k] = set(out[k]) | set(v)
                else:
                    for i in v:
                        out[k] = collect_values(i, out=out[k])
            elif type(v) == dict:
                out[k] = collect_values(v, out=out[k])
            else:
                out[k].add(v)
    return out


def remove_values(item, values):
    for k, v in list(item.items()):
        if k not in values:
            continue
        if type(v) not in [list, dict]:
            if v in values[k]:
                del item[k]
        elif type(v) == list:
            if len(v) > 0 and type(v[0]) not in [list, dict]:
                item[k] = (set(v) ^ values[k]) & set(v)
                if len(item[k]) == 0:
                    del item[k]
                else:
                    item[k] = list(item[k])
            else:
                for i in v:
                    remove_values(i, values[k])
        elif type(v) == dict:
            remove_values(v, values[k])
    return


def remove_values_all(data, values):
    data = [deepcopy(i) for i in data]
    for item in data:
        for k, v in list(item.items()):
            if k not in values:
                continue
            if type(v) not in [list, dict]:
                if v in values[k]:
                    del item[k]
            elif type(v) == list:
                if len(v) > 0 and type(v[0]) not in [list, dict]:
                    item[k] = (set(v) ^ values[k]) & set(v)
                    if len(item[k]) == 0:
                        del item[k]
                    else:
                        item[k] = list(item[k])
                else:
                    for i in v:
                        remove_values(i, values[k])
            elif type(v) == dict:
                remove_values(v, values[k])
    return data


def create_knowns(known, out=None):
    if out is None:
        out = {}
    if type(known) == list:
        for item in known:
            out = create_knowns(item, out=out)
    elif type(known) == dict:
        for k, v in known.items():
            if k not in out:
                if type(v) in [dict, list]:
                    out[k] = dict()
                else:
                    out[k] = set()
            if type(v) in [dict, list]:
                out[k] = create_knowns(v, out=out[k])
            else:
                out[k].add(v)
    else:
        if type(out) == dict:
            out = set()
        out.add(known)
    return out


def remove_knowns(known, unknown, agg=None, verbose=False):
    if agg is None:
        unknown = deepcopy(unknown)
        out = create_knowns(known)
    else:
        out = agg
    if type(unknown) == list:
        for item in unknown:
            for k in out:
                if k not in item:
                    continue
                if type(item[k]) == list:
                    if type(out[k]) == set:
                        item[k] = list((set(item[k]) ^ out[k]) & set(item[k]))
                    else:
                        remove_knowns(None, item[k], agg=out[k])
                    if len(item[k]) == 0:
                        del item[k]
                elif type(item[k]) == dict:
                    remove_knowns(None, item[k], agg=out[k])
                    if len(item[k]) == 0:
                        del item[k]
                else:
                    if item[k] in out[k]:
                        del item[k]
    else:
        for k in out:
            if k not in unknown:
                continue
            if type(unknown[k]) == list:
                if type(out[k]) == set:
                    unknown[k] = list((set(unknown[k]) ^ out[k]) & set(unknown[k]))
                else:
                    remove_knowns(None, unknown[k], agg=out[k])
                if len(unknown[k]) == 0:
                    del unknown[k]
            elif type(unknown[k]) == dict:
                remove_knowns(None, unknown[k], agg=out[k])
                if len(unknown[k]) == 0:
                    del unknown[k]
            else:
                if unknown[k] in out[k]:
                    del unknown[k]
    return unknown, out if verbose else unknown


def clean(data):
    if type(data) != dict:
        return
    for k, v in tuple(data.items()):
        if type(v) == dict:
            clean(v)
            if len(v) == 0:
                del data[k]
                continue
        if type(v) == list:
            for i in range(len(v) - 1, -1, -1):
                clean(v[i])
                if len(v[i]) == 0:
                    del v[i]
            if len(v) == 0:
                del data[k]
        if k in ['id', 'buyeruid', 'ext']:
            del data[k]
    return data


def flatten_dict(data, out=None, prepend=''):
    if out is None:
        out = {}
    for k, v in data.items():
        if type(v) == dict:
            flatten_dict(v, out=out, prepend=prepend+k+':')
            continue
        if type(v) == list and len(v) > 0 and type(v[0]) == dict:
            flatten_dict(v[0], out=out, prepend=prepend+k+'[0]:')
            continue
        out[prepend+k] = v
    return out
