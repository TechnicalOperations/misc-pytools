import re
import json
import queue
import threading
import numpy as np
from tools import sql
import snowflake.connector
from datetime import datetime, timedelta

float_re = re.compile('^[0-9\.]+$')


def make_segments(x):
    try:
        if type(x) == str:
            x = json.loads(x)
    except json.decoder.JSONDecodeError:
        return set([])
    x = [i.split(':') for i in x]
    for i in range(len(x)-1, -1, -1):
        try:
            x[i] = int(x[i][len(x[i])-1])
        except ValueError:
            del x[i]
        except IndexError:
            del x[i]
    x = list(set(x))
    x.sort()
    return tuple(x)


# The comp_ functions have agg as the DataFrame and deal as the actual value
#  Function name is comp_<agg type>_<deal type>
def comp_str_list(agg, column, deal, ids, list_type):
    if len(deal) == 0 or deal is None:
        return ids
    if list_type == 'exclude':
        new_ids = set(ids)
    else:
        new_ids = set()
    default = set()
    for deal_item in deal:
        if list_type == 'exclude':
            new_ids = (new_ids ^ agg[column].get(deal_item, default)) & new_ids
        else:
            new_ids = new_ids | agg[column].get(deal_item, default)
    return new_ids if list_type == 'exclude' else new_ids & ids


def comp_list_list(agg, column, deal, ids, list_type):
    if len(deal) == 0 or deal is None:
        return ids
    if list_type == 'exclude':
        new_ids = set(ids)
    else:
        new_ids = set()
    default = set()
    for deal_item in deal:
        if list_type == 'exclude':
            new_ids = (new_ids ^ agg[column].get(deal_item, default)) & new_ids
        else:
            new_ids = new_ids | agg[column].get(deal_item, default)
    return new_ids if list_type == 'exclude' else new_ids & ids


def comp_float_float(agg, column, deal, ids, *args):
    if deal is None or np.isnan(deal):
        return ids
    new_ids = set()
    for key, idx in agg[column].items():
        new_ids = new_ids if key < deal else new_ids | (ids & idx)
    return new_ids


class PmpFilter:

    SQL_DEAL_VIEW = """
        SELECT external_deal_id AS deal_id,
               ssp_name,
               dsp_name,
               device_filter,
               imp_filter,
               media_filter,
               ad_size_filter,
               ad_size_list,
               video_size_filter,
               video_size_list,
               country_filter,
               country_list,
               in_view_percentage,
               segment_id,
               filter_type AS segment_filter
        FROM ods.rx.deal_view
        WHERE active = '1'
            AND '{search_date}' >= deal_start
            AND '{search_date}' <= deal_end
        GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
    """

    DEAL_COLUMNS = [
        'deal_id',
        'ssp_name',
        'dsp_name',
        'device_filter',
        'imp_filter',
        'media_filter',
        'ad_size_filter',
        'ad_size_list',
        'video_size_filter',
        'video_size_list',
        'country_filter',
        'country_list',
        'in_view_percentage',
        'segment_id',
        'segment_filter'
    ]

    SQL_AGGREGATE = """
        SELECT event_date,
               ssp_id,
               dsps,
               device,
               imp_type,
               media_type,
               ad_size,
               IFF(video_w = '-', 'Unknown',
               IFF(TO_NUMERIC(video_w) <= 300, 'Small',
                IFF(TO_NUMERIC(video_w) <= 719, 'Medium',
                 IFF(TO_NUMERIC(video_w) <= 2159, 'Large', 'Extra Large'))) 
               ) AS video_size,
               country_code,
               ias_in_view_rate AS in_view_percentage,
               segments,
               SUM(requests) AS requests
        FROM r1_edw.rx.bidrequest_d_est
        WHERE event_date = '{{search_date}}'
            AND request_fail in ('nodspbids', 'bidresponse', '-')
        GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
        {limit}
    """

    AGGREGATE_COLUMNS = [
        'event_date',
        'ssp_id',
        'dsps',
        'device',
        'imp_type',
        'media_type',
        'ad_size',
        'video_size',
        'country_code',
        'in_view_percentage',
        'segments',
        'requests'
    ]

    # (deal type, agg type)
    COLUMNS = [
        ('ssp', {
            'agg': {'name': 'ssp_id', 'type': str},
            'deal': {'name': 'ssp_name', 'type': list}
        },),
        ('dsp', {
            'agg': {'name': 'dsps', 'type': list},
            'deal': {'name': 'dsp_name', 'type': list}
        },),
        ('device', {
            'agg': {'name': 'device', 'type': str},
            'deal': {'name': 'device_filter', 'type': list}
        },),
        ('imp_type', {
            'agg': {'name': 'imp_type', 'type': str},
            'deal': {'name': 'imp_filter', 'type': list}
        },),
        ('media_type', {
            'agg': {'name': 'media_type', 'type': str},
            'deal': {'name': 'media_filter', 'type': list}
        },),
        ('ad_size', {
            'agg': {'name': 'ad_size', 'type': str},
            'deal': {'name': 'ad_size_list', 'type': list, 'filter': 'ad_size_filter'}
        },),
        ('video_size', {
            'agg': {'name': 'video_size', 'type': str},
            'deal': {'name': 'video_size_list', 'type': list, 'filter': 'video_size_filter'}
        }),
        ('country_code', {
            'agg': {'name': 'country_code', 'type': str},
            'deal': {'name': 'country_list', 'type': list, 'filter': 'country_filter'}
        },),
        ('in_view_percentage', {
            'agg': {'name': 'in_view_percentage', 'type': float},
            'deal': {'name': 'in_view_percentage', 'type': float}
        },),
        ('segments', {
            'agg': {'name': 'segments', 'type': list, 'func': make_segments},
            'deal': {'name': 'segment_id', 'type': list, 'filter': 'segment_filter'}
        },)
    ]

    def lower_dict_keys(self, data):
        for item in data:
            keys = list(item.keys())
            for key in keys:
                item[key.lower()] = item[key]
                del item[key]
        return data

    def get_deals(self, search_date):
        sf_conn = sql.connect()
        if self.verbose:
            print("Reading deals from SnowFlake")
        sf_cur = sf_conn.cursor(snowflake.connector.DictCursor)
        self.deals = sf_cur.execute(
            PmpFilter.SQL_DEAL_VIEW.format(search_date=search_date.strftime('%Y-%m-%d'))
        ).fetchall()
        sf_cur.close()
        sf_conn.close()
        self.deals = self.lower_dict_keys(self.deals)
        for row in self.deals:
            for item in PmpFilter.COLUMNS:
                item = item[1]['deal']
                if item['type'] == list:
                    row[item['name']] = row[item['name']] if type(row[item['name']]) == str else '[]'
                    row[item['name']] = json.loads(row[item['name']])
                elif item['type'] == float:
                    row[item['name']] = np.nan if type(row[item['name']]) != str or float_re.search(row[item['name']]) is None else float(row[item['name']])
                if item.get('func', None) is not None:
                    row[item['name']] = item['func'](row[item['name']])
        if self.verbose:
            print("Deals Table Sample")
            print(self.deals[:10])

    def get_requests(self, search_date):
        sf_conn = sql.connect()
        sf_cur = sf_conn.cursor(snowflake.connector.DictCursor)
        if self.verbose:
            print("Reading requests from SnowFlake")
        requests = sf_cur.execute(
            PmpFilter.SQL_AGGREGATE.format(search_date=search_date.strftime('%Y-%m-%d'))
        )

        if self.verbose:
            print("Retrieving and cleaning data retrieved from SnowFlake")
        index = 0
        self.broken_requests = {
            col[1]['agg']['name']: dict()
            for col in PmpFilter.COLUMNS
        }
        self.requests = {}
        value_dump = {}
        while True:
            rows = requests.fetchmany(1000000)
            if self.verbose:
                print("Retrieved more rows")
            if rows is None or len(rows) == 0:
                break
            for row in rows:
                row['index'] = index
                row = {k.lower(): v for k, v in row.items()}
                for item in PmpFilter.COLUMNS:
                    item = item[1]['agg']
                    if row[item['name']] in value_dump:
                        row[item['name']] = value_dump[item['name']]
                    else:
                        if item['type'] == list:
                            row[item['name']] = row[item['name']] if type(row[item['name']]) == str else '[]'
                            row[item['name']] = row[item['name']] if type(row[item['name']]) == str else '[]'
                            try:
                                row[item['name']] = tuple(sorted(list(set(json.loads(row[item['name']])))))
                            except json.decoder.JSONDecodeError:
                                row[item['name']] = tuple([])
                        elif item['type'] == float:
                            row[item['name']] = np.nan if type(row[item['name']]) != str or float_re.search(row[item['name']]) is None else float(row[item['name']])
                        if item.get('func', None) is not None:
                            row[item['name']] = item['func'](row[item['name']])
                        value_dump[item['name']] = row[item['name']]
                    if type(row[item['name']]) == tuple:
                        for tuple_item in row[item['name']]:
                            if tuple_item not in self.broken_requests[item['name']]:
                                self.broken_requests[item['name']][tuple_item] = set()
                            self.broken_requests[item['name']][tuple_item].add(index)
                    else:
                        if row[item['name']] not in self.broken_requests[item['name']]:
                            self.broken_requests[item['name']][row[item['name']]] = set()
                        self.broken_requests[item['name']][row[item['name']]].add(index)
                self.requests[index] = row['requests']
                index += 1
        sf_cur.close()
        sf_conn.close()
        self.total = sum(self.requests.values())
        if self.verbose:
            print("Sub tables created")
            for k, v in self.broken_requests.items():
                print(k)
                print({i: list(j)[:5] for i, j in v.items()})

    def make_results_template(self):
        columns = [i[1]['agg']['name'] for i in PmpFilter.COLUMNS]
        for i in range(len(columns)-1, -1, -1):
            if columns[i] in columns[:i]:
                del columns[i]
        self.results = {i['deal_id']: {col: 0 for col in columns} for i in self.deals}
        for k, v in self.results.items():
            v['post_rg'] = self.total
        if self.verbose:
            print("Results template")
            print(self.results)

    def __init__(self, search_date=None, verbose=False, limit_requests=None):
        if search_date is None:
            search_date = datetime.now() - timedelta(days=1)
        if limit_requests is None:
            PmpFilter.SQL_AGGREGATE = PmpFilter.SQL_AGGREGATE.format(limit='')
        else:
            PmpFilter.SQL_AGGREGATE = PmpFilter.SQL_AGGREGATE.format(limit='LIMIT {0}'.format(limit_requests))
        search_date = search_date.strftime('%Y-%m-%d')
        search_date = datetime.strptime(search_date, '%Y-%m-%d')
        self.verbose = verbose
        self.deals = None
        self.get_deals(search_date)
        self.requests = None
        self.broken_requests = None
        self.total = 0
        self.get_requests(search_date)
        self.results = None
        self.make_results_template()

    def process(self, deal_id=None, number_of_threads=20):
        if number_of_threads is None or type(number_of_threads) != int:
            number_of_threads = len(self.results)
        results = queue.Queue()
        threads = []
        if self.verbose:
            print("Creating threads")
        deals_in = queue.Queue()
        for deal_row in self.deals:
            if deal_id is not None and deal_row['deal_id'] not in deal_id:
                continue
            deals_in.put(deal_row)
        for _ in range(number_of_threads):
            thread = threading.Thread(target=self.make_result, args=(deals_in, results,))
            thread.start()
            threads.append(thread)
        if self.verbose:
            print("Waiting for threads to complete")
        for thread in threads:
            thread.join()
        if self.verbose:
            print("Building out results")
        while not results.empty():
            result = results.get()
            results.task_done()
            for column, value in result['result'].items():
                self.results[result['deal_id']][column] = value
        for k, v in self.results.items():
            v['deal_id'] = k
        while not results.empty():
            result = results.get()
            print(result)
            for k, v in result['result'].items():
                self.results[result['deal_id']][k] = v

    def make_result(self, deals_in, results):
        while not deals_in.empty():
            deal_row = deals_in.get()
            if self.verbose:
                print("Processing deal {0}".format(deal_row['deal_id']))
            index = set([i for i in range(len(self.requests))])
            result = {'deal_id': deal_row['deal_id'], 'result': {}}
            for item in PmpFilter.COLUMNS:
                deal = deal_row[item[1]['deal']['name']]
                list_type = item[1]['deal'].get('filter', None)
                list_type = list_type if list_type is None else deal_row[list_type]
                if item[1]['agg']['type'] == item[1]['deal']['type']:
                    if item[1]['deal']['type'] == float:
                        index = comp_float_float(
                            self.broken_requests, item[1]['agg']['name'], deal, index, list_type
                        )
                    else:
                        index = comp_list_list(
                            self.broken_requests, item[1]['agg']['name'], deal, index, list_type
                        )
                else:
                    index = comp_str_list(
                        self.broken_requests, item[1]['agg']['name'], deal, index, list_type
                    )
                if len(index) > 0:
                    s = 0
                    for idx in index:
                        s += self.requests[idx]
                else:
                    break
                result['result'][item[1]['agg']['name']] = s
            results.put(result)

    def upload_results(self, connection, schema, table):
        cursor = connection.cursor()
        q = """
            INSERT INTO {schema}.{table}
            ({columns})
            VALUES
            ({values})
        """
        results = list(self.results.values())
        day = datetime.now() - timedelta(hours=24)
        day = day.strftime('%Y-%m-%d')
        results = [tuple([row[col[1]['agg']['name']] for col in PmpFilter.COLUMNS]+[day]) for row in results]
        columns = list([col[1]['agg']['name'] for col in PmpFilter.COLUMNS])
        columns.append('day')
        values = ['%s'] * len(columns)
        values[0] = '%s'
        values[len(values)-1] = '%s'
        values = ', '.join(values)
        q = q.format(
            schema=schema,
            table=table,
            columns=', '.join(columns),
            values=values
        )
        # cursor.executemany(q, results)
        # connection.commit()
        cursor.close()


if __name__ == "__main__":
    import pandas
    import MySQLdb
    from copy import deepcopy
    pandas.set_option('display.width', 5000)
    start = datetime.now()
    pmp_filter = PmpFilter(verbose=True)
    pmp_filter.process()
    con = MySQLdb.connect(read_default_file='/Users/cbeecher/.auth/.my.cnf', db='dashboard')
    pmp_filter.upload_results(con, 'dashboard', 'filters_pmpfunnel')
    con.close()
    data = []
    for deal, result in pmp_filter.results.items():
        result = deepcopy(result)
        result['deal_id'] = deal
        data.append(result)
    data = pandas.DataFrame(data)
    data = data[['deal_id', 'post_rg'] + [i[1]['agg']['name'] for i in PmpFilter.COLUMNS]]
    print(data[:30])
    print("\nTotal processing time: {0}".format(datetime.now() - start))
    # pmp_filter.results.to_csv(os.path.expanduser('~') + '/output.csv')
