import os
import re
import json
import random
import requests
import traceback
import tldextract
import threading
import queue
import gzip
from tools import sql
from datetime import datetime
from boto3 import session

processed = set()
html = re.compile('<.*?html|<.*?\?.*?xml|<.*?script|<.*?meta|<.*?head', re.I)
doc_version = "1.532"
doc_type = 'adstxt_crawler'

header = {
    'User-Agent': None
}

ua = [
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20130401 Firefox/31.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
]

domain_query = """
    WITH domains AS (
        SELECT site_domain, SUM(requests) AS num_requests
        FROM r1_edw.rx.bidrequest_d_est
        WHERE event_date > DATEADD(day, -2, current_timestamp())
        GROUP BY 1
        HAVING num_requests > 5000
        UNION
        SELECT raw_json:domain::string, 1
        FROM ods_stage.rx.adstxt_crawler_raw
        WHERE raw_json:has_adstxt = 1
        GROUP BY raw_json:domain, 2
    )
    SELECT DISTINCT site_domain AS site_domain
    FROM domains
    WHERE site_domain IS NOT NULL
"""


def extract_domain(domain):
    ext = tldextract.extract(domain)
    domain = '.'.join(ext[len(ext) - 2:])
    if domain in processed:
        return None
    processed.add(domain)
    return domain


def ping(domain, timeout, padding):
    header['User-Agent'] = random.choice(ua)
    try:
        data = requests.get('https://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout)
        if html.search(data.text.lower()) is not None or data.status_code != 200:
            data = requests.get('http://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout)
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
        try:
            data = requests.get('http://' + domain + '/ads.txt', allow_redirects=True, headers=header, timeout=timeout+padding)
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout) as e:
            if 'www.' not in domain:
                return ping('www.'+domain, timeout, padding)
            else:
                raise e
    return data


def get_adstxt(domain, timeout, padding):
    domain = extract_domain(domain)
    if domain is None:
        return -1
    data = ping(domain, timeout, padding)
    data.close()
    if data.status_code == 200 and html.search(data.text.lower()) is None and data.text.strip() != '':
        return re.sub(r'[^\x00-\x7f]', r'', data.text)
    return None


def enumerate_exceptions(string):
    if 'TooManyRedirects' in string:
        return 'redirects'
    if 'ChunkedEncodingError' in string:
        return 'chunkencoding'
    if 'ConnectionError' in string:
        return 'connection'
    if 'LocationValueError' in string:
        return 'location'
    if 'ContentDecodingError' in string:
        return 'contentdecoding'
    if 'SSLError' in string:
        return 'ssl'
    if 'UnicodeError' in string:
        return 'unicodeerror'
    return


def write_to_file(file, item):
    with gzip.open(file, 'ab') as file:
        out = json.dumps(item) + '\n'
        file.write(out.encode('utf-8'))


def build_item(domain, ads, error):
    time = datetime.now()
    time = int(time.timestamp() * 1000)
    item = {
        'domain': domain,
        'has_adstxt': 1 if ads is not None else 0,
        'adstxt_data': ads,
        'errors': None if error is None else enumerate_exceptions(error),
        'has_r1_adstxt': 1 if ads is not None and ('1rx' in ads.lower() or 'rhythmone' in ads.lower()) else 0,
        'doc_type': doc_type,
        'doc_version': doc_version,
        'time_server': time
    }
    return item


def crawler(file, domain_q, timeout, padding):
    while not domain_q.empty():
        domain = domain_q.get()
        domain_q.task_done()
        ads = None
        try:
            ads = get_adstxt(domain, timeout, padding)
            if ads == -1:
                continue
            error = None
        except:
            error = ''.join(traceback.format_exc())
        item = build_item(domain, ads, error)
        write_to_file(file, item)
    return None


def build_domain_queue():
    domain_q = queue.Queue()
    con = sql.connect()
    cur = con.cursor()
    domains = cur.execute(domain_query).fetchall()
    cur.close()
    sql.close()
    domains = [i[0] for i in domains]
    for domain in domains:
        domain_q.put(domain)
    return domain_q


def build_domain_queue_from_csv(file):
    domain_q = queue.Queue()
    with open(file, 'r') as f:
        domains = f.read().split('\n')
    domains = [i.strip() for i in domains if i.strip() != '']
    for domain in domains:
        domain_q.put(domain)
    return domain_q


def build_and_run_threads(number_of_threads, output, domain_q, timeout, padding):
    threads = []
    for i in range(number_of_threads):
        t = threading.Thread(target=crawler, args=(output, domain_q, timeout, padding,))
        t.start()
        threads.append(t)
    return threads


def upload_to_s3(file):
    sess = session.Session()
    client = sess.client('s3')

    date = datetime.utcnow()
    folder = "adstxt_crawler/{year}/{month}/{day}/{hour}/{{file}}"
    folder = folder.format(year=date.year, month=date.month, day=date.day, hour=date.hour)
    name = file.split('/')
    name = name[len(name)-1]

    client.upload_file(file, 'staging-rx-data', folder.format(file=name))
    os.remove(file)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    output = '~/output/adstxt_{0}.gz'.format(datetime.utcnow().strftime('%Y_%m_%d_%H'))
    parser.add_argument("--output", help="File to store the GZIP file", type=str, default=output)
    parser.add_argument("--input", help="New line delimited input file.  Default None to read from SF", default=None, type=str)
    parser.add_argument("--threads", help="Number of threads to create", type=int, default=10)
    parser.add_argument("--timeout", help="Scraper timeout in seconds", type=int, default=3)
    parser.add_argument("--padding", help="Additional time for the timeout on retry", type=int, default=10)
    args = parser.parse_args()

    threads = args.threads
    input = args.input
    output = args.output.replace('~', os.path.expanduser('~'))
    timeout = args.timeout
    padding = args.padding

    if input is None:
        domain_q = build_domain_queue()
    else:
        input = input.replace('~', os.path.expanduser('~'))
        domain_q = build_domain_queue_from_csv(input)
    threads = build_and_run_threads(threads, output, domain_q, timeout, padding)

    for thread in threads:
        thread.join()

    upload_to_s3(output)
