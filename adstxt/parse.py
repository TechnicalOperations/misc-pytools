import re
import pandas
pandas.set_option('display.width', 5000)


rs = re.compile('\r|\n|\\\\+r|\\\\+n')
rc = re.compile('#.*?$', re.M)
RESELLER = ['receller', 'reseller', 'resseler']
DIRECT = ['direct']
MANAGED = ['managed']


def format(x):
    domain = x['domain']
    x = x['adstxt']
    x = rs.split(x)
    for i in range(len(x)-1, -1, -1):
        m = rc.search(x[i])
        m = m.group(0).replace('#', '').strip() if m is not None else ''
        x[i] = rc.sub('', x[i])
        x[i] = x[i].strip()
        if len(x[i]) == 0:
            del x[i]
            continue
        x[i] = x[i].split(',')
        x[i] = [z.strip() for z in x[i]]
        if len(x[i]) > 2 and len(x[i]) < 5:
            x[i] = {'seller': x[i][0].strip(), 'pub_id': x[i][1].strip(), 'rel': x[i][2].strip(), 'domain': domain, 'comment': m}
        else:
            del x[i]
    return x


def find_rel(x):
    if True in [i in x.lower() for i in MANAGED]:
        return 'MANAGED'
    if True in [i in x.lower() for i in DIRECT]:
        return 'DIRECT'
    if True in [i in x.lower() for i in RESELLER]:
        return 'RESELLER'
    return ''


# domain, adstxt, has ads txt, Has 1R
def parse_adstxt(input, parse_output, agg_output=None):
    data = pandas.read_csv(input)
    data = data[~data['adstxt'].str.contains('<[^ ]+.*?/[^ ]+>')].reset_index(drop=True)
    data['adstxt'] = data.apply(format, axis=1)
    data = list(data['adstxt'])
    data = [z for i in data for z in i]
    data = pandas.DataFrame(data)[['domain', 'seller', 'pub_id', 'rel', 'comment']]
    data['rel'] = data['rel'].str.replace('\\\\[a-zA-Z]', '').str.replace('[^a-zA-Z]', '')
    data['rel'] = data['rel'].apply(find_rel)
    data = data[~data['rel'].apply(lambda x: x == '')].reset_index(drop=True)
    data['seller'] = data['seller'].apply(lambda x: x.lower())
    
    agg = data.groupby('seller').agg(lambda x: len(set(x))).sort_values(by='pub_id', ascending=False)
    print(agg[:30])
    
    print('')
    print("Number of entries: {0}".format(len(data)))
    print("Number of unique domains: {0}".format(len(set(data['domain']))))
    print("Number of unique pub IDs: {0}".format(len(set(data['pub_id']))))

    data[data['seller'].str.contains('openx')].to_excel(parse_output, index=False)
    if agg_output is not None:
        agg.to_excel(agg_output)
    return data


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        raise Exception("Need to specify minimum of input and output")
    input = sys.argv[1]
    output = sys.argv[2]
    agg = sys.argv[3] if len(sys.argv) > 3 else None
    parse_adstxt(input, output, agg_output=agg)
