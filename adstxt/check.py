import re
import requests
import multiprocessing as mp
import argparse
from ua_gen import RandomUA
import urllib.request
import argparse


# Get dates
def get_date():
    year = int(time.strftime("%Y"))
    month = int(time.strftime("%m"))
    day = int(time.strftime("%d"))

    today = datetime.date(year, month, day)
    offset = datetime.timedelta(days=1)
    yesterday = today - offset

    sql_dates = [yesterday, today]
    return sql_dates

def db_ins(results):
    time = results[0]
    site = results[1]
    tf = results[2]
    text = results[3]
    r1_adstxt = results[4]

    print("Site: {}".format(site))
    print("Adstxt: {}".format(tf))
    print("R1 Adstxt: {}".format(r1_adstxt))
    print("-----\n\n{}".format(text))

    return


# HTTP/S Request
def get_req(n):
    # Pull User-Agent String
    rua = RandomUA()
    ua = rua.ua_gen()

    j = []
    host = "http://{}/ads.txt".format(n)

    # Disregard SSL Warnings
    requests.packages.urllib3.disable_warnings()

    # Result list
    qlist = []

    # Default 1R Ads.txt value
    r1_adstxt = 0

    # Issue HTTP request
    try:
        recheck = True
        check_limit = 0
        while recheck:
            check_limit += 1
            if check_limit > 2:
                break

            headers = {'User-Agent':ua}
            req = requests.get(host, headers=headers, allow_redirects=False, timeout=3)
            sc = str(req.status_code)

            if sc == "200":
                if req.text != None:
                    html_check = re.search("<.*?html|<.*?\?.*?xml|<.*?script|<.*?meta", req.text.lower())
                    if not html_check:
                        r1_check = re.search("rhythmone", req.text.lower())
                        if r1_check:
                            r1_adstxt = 1
                        qlist.append(0)
                        qlist.append(n)
                        qlist.append(1)
                        qlist.append(req.text)
                        qlist.append(r1_adstxt)
                        return qlist
            elif sc.startswith("3"):
                host = "{}".format(req.headers['Location'])
            else:
                qlist.append(0)
                qlist.append(n)
                qlist.append(0)
                qlist.append("")
                qlist.append(r1_adstxt)
                return qlist
    except:
        qlist.append(0)
        qlist.append(n)
        qlist.append(0)
        qlist.append("")
        qlist.append(r1_adstxt)
        return qlist
    return



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--site", help="Target Website", required=True)
    args = vars(parser.parse_args())
    site_check = args['site']

    sk = get_req(site_check)
    db_ins(sk)
